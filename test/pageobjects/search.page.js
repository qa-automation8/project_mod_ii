const Page = require('./page');

class SearchPage extends Page {

    pageSearchUrl        = 'search?sp=all&q=';
    specialityOption     = '//a[contains(text(),"[speciality]")]';

    // Selectors
    get divCardTitle      () { return $(`//h3[contains(text(),'Maria')]`) };
    get inputSearch       () { return $('.form-control') };
    get inputSubmitSearch () { return $('[value=Buscar]') };
    get divCardContainer  () { return $(`//div[@class='col-lg-7']//descendant::div[1]//h3[contains(text(), 'Maria')]`) };
    get iSwitchListMap    () { return $(`//div[@class='layout_view']//a`) };
    get asideEmptyMap     () { return $(`//aside[@class='d-none']`) };
    
    // Construct a speciality option type a.
    aButtonSpeciality(speciality) {
        return $(this.specialityOption.replace("[speciality]", speciality));
    }

    // Click on three different speciality options.
    clickSpecialityOption(speciality){
        this.aButtonSpeciality(speciality).click();
    }

    // Search method with data.
    searchSpecialist(searchData){
        this.inputSearch.setValue(searchData);
        this.inputSubmitSearch.click();
    }

    // Click map option.
    clickMapOption(){
        this.iSwitchListMap.click();
    }

    // URL method for navigating the search page.
    open () {
        return super.open('https://develop.terapeutica.digital/#/search?q=');
    }
}

module.exports = new SearchPage();