const Page = require('./page');

class ProfilePage extends Page {

    // URL method for navigating the home page.
    open () {
        return super.open('https://develop.terapeutica.digital/#/specialist/37378b04-4b69-452e-9fad-e83959388f41');
    }
}

module.exports = new ProfilePage();