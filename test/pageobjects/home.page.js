const Page = require('./page');

class HomePage extends Page {

    pageHomeUrl = 'https://develop.terapeutica.digital/#/';

    // Selectors
    get btnSearch       () { return $('[value=Buscar]') };
    get mainTitle       () { return $('h3=¿Buscas un Terapeuta?') };
    get lblPhisical     () { return $('[for="phisical"]') };
    get lblLanguage     () { return $('[for="language"]') };
    get lblOcupational  () { return $('[for="ocupational"]') };
    get divSearch       () { return $('.input-group') };
    get inputSearch     () { return $('#search-input') };

    // Search method with empty data.
    search () {
        this.btnSearch.click();
    }

    // Click on three different labels.
    clickPhisicalLabel(){
        this.lblPhisical.click();
    }

    clickLanguageLabel(){
        this.lblLanguage.click();
    }

    clickOcupationalLabel(){
        this.lblOcupational.click();
    }

    // Search method with data.
    searchSpecialist(searchData){
        this.inputSearch.setValue(searchData);
        this.btnSearch.click();
    }

    // URL method for navigating the home page.
    open () {
        return super.open('/');
    }
}

module.exports = new HomePage();