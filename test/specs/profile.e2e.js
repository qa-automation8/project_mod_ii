const ProfilePage = require('../pageobjects/profile.page');

describe('Profile Page Funtions', () => {

    before( () => {
        ProfilePage.open();
    })

    // Verificar que el ​webservice​ correcto es llamado al ingresar a la página de detalles de perfil de un profesional.

    it('web service is correct', () => {

        let serverRequest = 'https://javito-stage.herokuapp.com/v1/specialist/37378b04-4b69-452e-9fad-e83959388f41';

        browser.setupInterceptor();
        browser.expectRequest('GET', serverRequest, 200);
        
    });

});