const { specialitiesData, specialistData } = require('../helpers/search.helper');
const SearchPage           = require('../pageobjects/search.page');

describe('Search Page Funtions', () => {


    beforeEach( () => {
        SearchPage.open();
    })

    // Validar que al cambiar entre las diferentes ​especialidades​ la URL cambia para reflejar la especialidad seleccionada.

    it('clicking the speciality options, url is correct', () => {

        let specialities = specialitiesData();

        for (let index = 0; index < specialities.length; index++) {

            switch (index) {
                case 0:
                    SearchPage.clickSpecialityOption(specialities[0]['name']);
                    expect(browser).toHaveUrl(specialities[0]['url']);
                    break;
                case 1:
                    SearchPage.clickSpecialityOption(specialities[1]['name']);
                    expect(browser).toHaveUrl(specialities[1]['url']);
                    break;
                case 2:
                    SearchPage.clickSpecialityOption(specialities[2]['name']);
                    expect(browser).toHaveUrl(specialities[2]['url']);
                    break;
                default:
                    break;
            }

        }

        // browser.debug();
        
    });

    // Validar que al ingresar el texto ​Maria​, y hacer clic en ​Buscar ​la pagina es refrescada y el primer resultado es un especialista con el nombre de ​Maria

    it('clicking the search button with text Maria, the first result is a specialist with Maria name', () => {

        let specialist = specialistData();

        SearchPage.searchSpecialist(specialist['name']);
        expect(browser).toHaveUrl(specialist['url']);
        expect(SearchPage.divCardContainer).toExist();
        expect(SearchPage.divCardContainer).toBeDisplayed();

        // browser.debug();
        
    });

    // Validar que al cambiar entre mapa y lista el mapa desaparece de la página.

    it('switching between map and list the map disappears from the page', () => {

        SearchPage.clickMapOption();
        expect(SearchPage.asideEmptyMap).toExist();

        // browser.debug();
        
    });

});