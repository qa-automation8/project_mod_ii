const HomePage   = require('../pageobjects/home.page');
const SearchPage = require('../pageobjects/search.page');

describe('Home Page Funtions', () => {


    beforeEach( () => {
        HomePage.open();
    })

    // Validar que al hacer clic en el botón de buscar sin ingresar texto el sitio no redirige al usuario a ninguna pagina.

    it('clicking the search button does not redirect to another page', () => {

        HomePage.search();
        expect(HomePage.mainTitle).toExist();
        expect(browser).toHaveUrl(HomePage.pageHomeUrl);

        // browser.debug();
        
    });

    // Validar que al hacer clic en una especialidad el campo de búsqueda obtiene el foco y el placeholder del mismo cambia a: ​Buscas a alguien o algo en especifico.

    it('clicking the speciality label, search box gets focus', () => {

        const placeholder = "¿Buscas a alguien o algo en específico?";

        for (let index = 0; index < 3; index++) {

            switch (index) {
                case 0:
                    HomePage.clickPhisicalLabel();
                    expect(HomePage.divSearch).toHaveClassContaining('focus-div');
                    expect(HomePage.inputSearch).toBeFocused();
                    expect(HomePage.inputSearch).toHaveAttribute('placeholder', placeholder);
                    HomePage.open();
                    break;
                case 1:
                    HomePage.clickLanguageLabel();
                    expect(HomePage.divSearch).toHaveClassContaining('focus-div');
                    expect(HomePage.inputSearch).toBeFocused();
                    expect(HomePage.inputSearch).toHaveAttribute('placeholder', placeholder);
                    HomePage.open();
                    break;
                case 2:
                    HomePage.clickOcupationalLabel();
                    expect(HomePage.divSearch).toHaveClassContaining('focus-div');
                    expect(HomePage.inputSearch).toBeFocused();
                    expect(HomePage.inputSearch).toHaveAttribute('placeholder', placeholder);
                    break;
                default:
                    break;
            }

        }

        // browser.debug();

    });

    // Validar que al ingresar el texto ​Maria​, y hacer clic en ​Buscar ​el usuario es dirigido a la página de resultados y el primero resultado es un especialista con el nombre de ​Maria​.

    it('clicking the search button with text, it is redirect to search page', () => {

        const searchData = 'Maria';

        HomePage.searchSpecialist(searchData);
        expect(browser).toHaveUrlContaining(SearchPage.pageSearchUrl);
        expect(SearchPage.divCardTitle).toExist();

        // browser.debug();
        
    });

});