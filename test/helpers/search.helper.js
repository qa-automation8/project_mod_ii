
function specialitiesData() {
    return [
        { name: 'Física',      url: 'https://develop.terapeutica.digital/#/search?q=&sp=phisical' },
        { name: 'Lenguaje',    url: 'https://develop.terapeutica.digital/#/search?q=&sp=language' },
        { name: 'Ocupacional', url: 'https://develop.terapeutica.digital/#/search?q=&sp=ocupational' }
    ];
};

function specialistData() {
    return { 
        name: 'Maria',      
        url: 'https://develop.terapeutica.digital/#/search?q=Maria' 
    };
};

module.exports = { 
    specialitiesData,
    specialistData
}